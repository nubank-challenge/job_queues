defmodule JobQueues.AgentTest do
  use ExUnit.Case, async: false

  setup do
    JobQueues.Agent.clean()
  end

  describe ".insert_or_update/1" do
    test "should answer with error when try insert agent with nil id" do
      assert JobQueues.Agent.find(1) == nil
      assert JobQueues.Agent.insert_or_update(%{"id" => nil, "name" => "Invalid"}) == {:error, :invalid_agent_id}
      assert JobQueues.Agent.find(1) == nil
    end

    test "should answer with error when try insert agent with invalid payload" do
      assert JobQueues.Agent.find(1) == nil
      assert JobQueues.Agent.insert_or_update([{"id", 1}, {"name", "Invalid"}]) == {:error, :invalid_agent}
      assert JobQueues.Agent.find(1) == nil
    end

    test "should answer with :ok when try insert agent with valid payload" do
      agent_payload = %{"id" => 1, "name" => "Teste"}
      assert JobQueues.Agent.find(1) == nil
      assert JobQueues.Agent.insert_or_update(agent_payload) == :ok
      assert JobQueues.Agent.find(1) == agent_payload
    end

    test "should answer with :ok when try update agent with valid payload" do
      agent_payload = %{"id" => 1, "name" => "Teste"}
      agent_payload_modified = %{"id" => 1, "name" => "Teste Modificado"}
      assert JobQueues.Agent.find(1) == nil
      assert JobQueues.Agent.insert_or_update(agent_payload) == :ok
      assert JobQueues.Agent.find(1) == agent_payload
      assert JobQueues.Agent.insert_or_update(agent_payload_modified) == :ok
      assert JobQueues.Agent.find(1) == agent_payload_modified
    end
  end

  describe ".all/0" do
    test "should answer with empty list when don't exists current agents" do
      assert JobQueues.Agent.all() == []
    end

    test "should answer with list when exists current agents" do
      agent_payload1 = %{"id" => 1, "name" => "Teste 1"}
      agent_payload2 = %{"id" => 2, "name" => "Teste 2"}
      assert JobQueues.Agent.all() == []
      assert JobQueues.Agent.insert_or_update(agent_payload1) == :ok
      assert JobQueues.Agent.insert_or_update(agent_payload2) == :ok
      assert JobQueues.Agent.all() == [agent_payload1, agent_payload2]
    end
  end

  describe ".find/1" do
    test "should answer with nil when try find agent with nil id" do
      assert JobQueues.Agent.find(nil) == nil
    end

    test "should answer with nil when agent don't exists" do
      assert JobQueues.Agent.find(1) == nil
    end

    test "should answer with agent payload when agent exists" do
      agent_payload = %{"id" => 1, "name" => "Teste"}
      assert JobQueues.Agent.find(1) == nil
      assert JobQueues.Agent.insert_or_update(agent_payload) == :ok
      assert JobQueues.Agent.find(1) == agent_payload
    end
  end

  describe ".delete/1" do
    test "should answer with nil when try find agent with nil id" do
      assert JobQueues.Agent.delete(nil) == nil
    end

    test "should answer with nil when agent don't exists" do
      assert JobQueues.Agent.delete(1) == nil
    end

    test "should answer with :ok when agent exists" do
      agent_payload = %{"id" => 1, "name" => "Teste"}
      assert JobQueues.Agent.find(1) == nil
      assert JobQueues.Agent.insert_or_update(agent_payload) == :ok
      assert JobQueues.Agent.find(1) == agent_payload
      assert JobQueues.Agent.delete(1) == agent_payload
      assert JobQueues.Agent.find(1) == nil
    end
  end

  describe ".clean/0" do
    test "should answer with :ok when try clean current empty agents" do
      assert JobQueues.Agent.clean() == :ok
    end

    test "should answer with :ok when try clean current agents" do
      agent_payload = %{"id" => 1, "name" => "Teste"}
      assert JobQueues.Agent.find(1) == nil
      assert JobQueues.Agent.insert_or_update(agent_payload) == :ok
      assert JobQueues.Agent.find(1) == agent_payload
      assert JobQueues.Agent.clean() == :ok
      assert JobQueues.Agent.find(1) == nil
    end
  end
end