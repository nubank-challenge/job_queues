defmodule JobQueues.Job do
  alias Agent, as: ElixirAgent
  use ElixirAgent

  @doc """
  Starts JobQueues.Job.
  """
  def start_link(_opts) do
    state = for job_classification <- job_classifications(), into: %{}, do: {job_classification, %{}}
    ElixirAgent.start_link(fn -> state end, name: __MODULE__)
  end

  @doc """
  Insert a Job identified by `id`.

  Returns `:ok`, `{:error, :invalid_id}`, `{:error, :invalid_job}` or `{:error, :job_already_exists}`
  """
  def insert(%{"id" => id, "urgent" => true} = job), do: insert_in_classification(id, job, :unassigned_urgent_jobs)
  def insert(%{"id" => id} = job), do: insert_in_classification(id, job, :unassigned_jobs)
  def insert(_), do: {:error, :invalid_job}

  @doc """
  Insert a Job identified by `id` ànd `job_classification`.

  Returns `:ok`, `{:error, :invalid_job_id}` or `{:error, :job_already_exists}`
  """
  def insert_in_classification(nil, _job, _job_classification), do: {:error, :invalid_job_id}
  def insert_in_classification(id, %{"id" => job_id} = job, job_classification) do
    if already_exists?(job_id) do
      {:error, :job_already_exists}
    else
      ElixirAgent.update(__MODULE__, fn (%{^job_classification => jobs} = state) ->
        jobs = Map.put(jobs, id, job)
        Map.put(state, job_classification, jobs)
      end)
    end
  end

  @doc """
  Recover job by `id`.

  Returns a Map with `job_classification` and `Job` or empty.
  """
  def find(nil), do: %{}
  def find(id) do
    find_job = fn (jobs) -> (jobs[id] || Map.values(jobs) |> Enum.find(&(&1["id"] == id))) end
    ElixirAgent.get(__MODULE__, fn (state) ->
      for {job_classification, jobs} <- state, find_job.(jobs) != nil, into: %{} do
        {job_classification, find_job.(jobs)}
      end
    end)
  end

  @doc """
  Recover job by `id` and `job_classification`.

  Returns a `Job` or `nil`.
  """
  def find_in_classification(nil, _job_classification), do: nil
  def find_in_classification(id, job_classification) do
    ElixirAgent.get(__MODULE__, fn (%{^job_classification => jobs}) -> Map.get(jobs, id) end)
  end

  @doc """
  Recover all current classificated jobs.

  Returns a Map with current classificated jobs or empty.
  """
  def all do
    ElixirAgent.get(__MODULE__, fn (state) ->
      for {job_classification, jobs} <- state, into: %{} do
        {job_classification, Map.values(jobs)}
      end
    end)
  end

  @doc """
  Recover jobs by `job_classification`.

  Returns a List with jobs or empty.
  """
  def all(job_classification) do
    ElixirAgent.get(__MODULE__, fn (%{^job_classification => jobs}) -> Map.values(jobs) end)
  end

  @doc """
  Find a unnasigned urgent job and unnasigned job by `types`.

  Returns a Map with unnasigned classifications and jobs or nil.
  """
  def find_unassigned_job_in_types(types) do
    ElixirAgent.get(__MODULE__, fn (state) ->
      for {job_classification, jobs} <- state, Enum.member?(unassigned_job_classifications(), job_classification), into: %{} do
        job = case match_type?(jobs, types) do
          nil -> nil
          {_id, job} -> job
        end
        {job_classification, job}
      end
    end)
  end

  @doc """
  Find a assigned job by `agent_id`.

  Returns a Job or nil.
  """
  def find_assigned_job_by_agent_id(nil), do: nil
  def find_assigned_job_by_agent_id(agent_id) do
    ElixirAgent.get(__MODULE__, fn (%{assigned_jobs: jobs}) ->
      Map.get(jobs, agent_id)
    end)
  end

  @doc """
  Recover done jobs by `agent_id`.

  Returns a List with done jobs or empty.
  """
  def find_done_jobs_by_agent_id(nil), do: []
  def find_done_jobs_by_agent_id(agent_id) do
    ElixirAgent.get(__MODULE__, fn (%{done_jobs: jobs}) ->
      jobs
      |> Map.values
      |> Enum.filter(fn(job) -> match?(%{"agent_id" => ^agent_id}, job) end)
    end)
  end

  @doc """
  Ends a assigned_job by `agent_id`.

  Returns `:ok`, `{:error, :invalid_agent_id}`, `{:error, :invalid_job}` or `{:error, :invalid_job_id}`.
  """
  def ends_assigned_job(nil, _job), do: {:error, :invalid_agent_id}
  def ends_assigned_job(_agent_id, nil), do: {:error, :invalid_job}
  def ends_assigned_job(_agent_id, %{"id" => nil}), do: {:error, :invalid_job_id}
  def ends_assigned_job(agent_id, %{"id" => job_id} = job) do
    delete_in_classification(agent_id, :assigned_jobs)
    insert_in_classification(job_id, job, :done_jobs)
  end

  @doc """
  Delete a Job identified by `id`.

  Returns removed `:ok` or `nil`
  """
  def delete_in_classification(nil, _job_classification), do: nil
  def delete_in_classification(id, job_classification) do
    ElixirAgent.update(__MODULE__, fn (%{^job_classification => jobs} = state) ->
      {_removed, jobs} = Map.pop(jobs, id)
      Map.put(state, job_classification, jobs)
    end)
  end

  @doc """
  Clean current jobs.

  Returns `:ok`.
  """
  def clean do
    state = for job_classification <- job_classifications(), into: %{}, do: {job_classification, %{}}
    ElixirAgent.update(__MODULE__, fn _current_state -> state end)
  end

  # Private Methods

  defp job_classifications, do: [:unassigned_urgent_jobs, :unassigned_jobs, :assigned_jobs, :done_jobs]

  defp unassigned_job_classifications, do: [:unassigned_urgent_jobs, :unassigned_jobs]

  defp already_exists?(id) do
    job_classifications()
    |> Enum.any?(&find_in_classification(id, &1))
  end

  defp match_type?(jobs, types), do: Enum.find(jobs, fn({_id, %{"type" => type}}) -> Enum.member?(types, type) end)
end