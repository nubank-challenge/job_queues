defmodule JobQueues.Application do
  use Application

  def start(_type, _args) do
    {:ok, _pid} = JobQueues.Supervisor.start_link(name: JobQueues.Supervisor)
  end
end
